import network
import json
#import vga1_bold_16x32 as font
import vga1_8x16 as font
import tft_config
# https://github.com/russhughes/s3lcd
import s3lcd

def wlan_connect(ssid="MYSSID", password="MYPASSWORD"):
    """
    Connect to WiFi network
    """
    wlan = network.WLAN(network.STA_IF)
    if not wlan.active() or not wlan.isconnected():
        wlan.active(True)
        print("connecting to:", ssid)
        try:
            wlan.connect(ssid, password)
        except Exception as e:
            print(e)
        while not wlan.isconnected():
            pass
    print("network config:", wlan.ifconfig())

def http_get(url):
    """
    GET HTTP url
    https://docs.micropython.org/en/latest/esp8266/tutorial/network_tcp.html?highlight=http
    """
    import socket
    
    http_response = ""
    header = None
    body = None
    
    _, _, host, path = url.split('/', 3)
    addr = socket.getaddrinfo(host, 80)[0][-1]
    s = socket.socket()
    s.connect(addr)
    s.send(bytes("GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n" % (path, host), "utf8"))
    while True:
        data = s.recv(100)
        if data:
            http_response += str(data, "utf8")
            #print(str(data, "utf8"), end="")
        else:
            break
    s.close()
    
    # split header and body based on empty line
    header_body = http_response.split("\r\n\r\n")
    header = header_body[0]
    body = header_body[1]
    
    return header, body

def build_pattern(tft, font):
    """
    Build pattern that will be used when formattin active power strings
    """
    pattern = "{:<10}{:>20.2f} W"
    print("tft_width: {}".format(tft.width()))
    print("font width: {}".format(font.WIDTH))
    
    label_width = int( (tft.width()/font.WIDTH)/2 )
    value_width = int( (tft.width()/font.WIDTH)/2 ) - 2 # 2 for " W"
    
    pattern = "{{:<{}}}{{:>{}.2f}} W".format(label_width, value_width)
    print("{}".format(pattern))
    
    return pattern

def main():
    # connect to wifi
    wlan_connect()

    # get basic status from shelly device
    shelly_ip_address = "192.168.1.142"
    url = f"http://{shelly_ip_address}/rpc/EM.GetStatus?id=0"
    header, body = http_get(url)

    # convert json to dict
    response = json.loads(body)

    try:
        # init tft screen
        tft = tft_config.config(tft_config.WIDE)
        tft.init()
        
        # background and foreground colours
        fg = s3lcd.WHITE
        bg = s3lcd.BLACK

        # Shelly Pro 3 EM
        # https://shelly-api-docs.shelly.cloud/gen2/Devices/ShellyPro3EM/
        pattern = build_pattern(tft, font)
        
        label = "A active power:"
        value = response["a_act_power"]
        text = pattern.format(label, value)
        tft.text(font, text, 0, 20, fg, bg)

        label = "B active power:"
        value = response["b_act_power"]
        text = pattern.format(label, value)
        tft.text(font, text, 0, 50, fg, bg)

        label = "C active power:"
        value = response["c_act_power"]
        text = pattern.format(label, value)
        tft.text(font, text, 0, 80, fg, bg)
        
        # show display
        tft.show()

    finally:
        tft.deinit()


main()
