# Shelly Python

## Description

Several (Micro)Python files to access Shelly devices, using Shelly [Gen2 Device API](https://shelly-api-docs.shelly.cloud/gen2/).

